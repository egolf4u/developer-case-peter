# Developer case Peter

Zie dataset recepten.sql voor een relationele database met recepten per provincie.

**Opdracht**

Toon een overzicht van alle provincies. Als je op een provincie klikt, krijg je een overzicht te zien van alle recepten. Het overzicht moet je client side kunnen sorteren en zoeken naar een recept (op naam).

Je bent vrij om een php en/of css framework naar keuze te gebruiken. Voor het tonen van het overzicht mag je gebruik maken van een externe library.

**Homestead**

We hebben een Laravel Homestead box opgezet, waarin je kunt ontwikkelen. Zie voor meer informatie https://laravel.com/docs/5.7/homestead